angular.module('app.routes', ['ionicUIRouter'])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    

      .state('tabsController', {
    url: '/page1',
    templateUrl: 'templates/tabsController.html',
    abstract:true
  })

  .state('crearCuenta', {
    url: '/page5',
    templateUrl: 'templates/crearCuenta.html',
    controller: 'crearCuentaCtrl'
  })

  .state('modificarCuenta', {
    url: '/page10',
    templateUrl: 'templates/modificarCuenta.html',
    controller: 'modificarCuentaCtrl'
  })

  .state('login', {
    url: '/page6',
    templateUrl: 'templates/login.html',
    controller: 'loginCtrl'
  })

  .state('tabsController.triage1', {
    url: '/page7',
    views: {
      'tab1': {
        templateUrl: 'templates/triage1.html',
        controller: 'triage1Ctrl'
      }
    }
  })

  /* 
    The IonicUIRouter.js UI-Router Modification is being used for this route.
    To navigate to this route, do NOT use a URL. Instead use one of the following:
      1) Using the ui-sref HTML attribute:
        ui-sref='tabsController.turno'
      2) Using $state.go programatically:
        $state.go('tabsController.turno');
    This allows your app to figure out which Tab to open this page in on the fly.
    If you're setting a Tabs default page or modifying the .otherwise for your app and
    must use a URL, use one of the following:
      /page1/tab1/page9
      /page1/tab2/page9
  */
  .state('tabsController.turno', {
    url: '/page9',
    views: {
      'tab1': {
        templateUrl: 'templates/turno.html',
        controller: 'turnoCtrl'
      },
      'tab2': {
        templateUrl: 'templates/turno.html',
        controller: 'turnoCtrl'
      }
    }
  })

  .state('tabsController.varios', {
    url: '/page11',
    views: {
      'tab3': {
        templateUrl: 'templates/varios.html',
        controller: 'variosCtrl'
      }
    }
  })

  .state('tabsController.abdominalesYGastrointestinales', {
    url: '/page12',
    views: {
      'tab1': {
        templateUrl: 'templates/abdominalesYGastrointestinales.html',
        controller: 'abdominalesYGastrointestinalesCtrl'
      }
    }
  })

  .state('tabsController.cardiovasculares', {
    url: '/page14',
    views: {
      'tab1': {
        templateUrl: 'templates/cardiovasculares.html',
        controller: 'cardiovascularesCtrl'
      }
    }
  })

  .state('tabsController.neurolGicos', {
    url: '/page29',
    views: {
      'tab1': {
        templateUrl: 'templates/neurolGicos.html',
        controller: 'neurolGicosCtrl'
      }
    }
  })

  .state('tabsController.oDo', {
    url: '/page16',
    views: {
      'tab1': {
        templateUrl: 'templates/oDo.html',
        controller: 'oDoCtrl'
      }
    }
  })

  .state('tabsController.ojos', {
    url: '/page17',
    views: {
      'tab1': {
        templateUrl: 'templates/ojos.html',
        controller: 'ojosCtrl'
      }
    }
  })

  .state('tabsController.narizBocaYGarganta', {
    url: '/page18',
    views: {
      'tab1': {
        templateUrl: 'templates/narizBocaYGarganta.html',
        controller: 'narizBocaYGargantaCtrl'
      }
    }
  })

  .state('tabsController.musculoesquelTico', {
    url: '/page24',
    views: {
      'tab1': {
        templateUrl: 'templates/musculoesquelTico.html',
        controller: 'musculoesquelTicoCtrl'
      }
    }
  })

  .state('tabsController.respiratorios', {
    url: '/page15',
    views: {
      'tab1': {
        templateUrl: 'templates/respiratorios.html',
        controller: 'respiratoriosCtrl'
      }
    }
  })

  .state('tabsController.urinarios', {
    url: '/page30',
    views: {
      'tab1': {
        templateUrl: 'templates/urinarios.html',
        controller: 'urinariosCtrl'
      }
    }
  })

  .state('tabsController.psiquiTricos', {
    url: '/page33',
    views: {
      'tab1': {
        templateUrl: 'templates/psiquiTricos.html',
        controller: 'psiquiTricosCtrl'
      }
    }
  })

  .state('tabsController.pielYAnexos', {
    url: '/page34',
    views: {
      'tab1': {
        templateUrl: 'templates/pielYAnexos.html',
        controller: 'pielYAnexosCtrl'
      }
    }
  })

  .state('tabsController.otros', {
    url: '/page35',
    views: {
      'tab1': {
        templateUrl: 'templates/otros.html',
        controller: 'otrosCtrl'
      }
    }
  })

  .state('tabsController.genitalMasculino', {
    url: '/page20',
    views: {
      'tab1': {
        templateUrl: 'templates/genitalMasculino.html',
        controller: 'genitalMasculinoCtrl'
      }
    }
  })

  .state('tabsController.pacienteEmbarazada', {
    url: '/page21',
    views: {
      'tab1': {
        templateUrl: 'templates/pacienteEmbarazada.html',
        controller: 'pacienteEmbarazadaCtrl'
      }
    }
  })

  .state('tabsController.pacienteNoEmbarazada', {
    url: '/page22',
    views: {
      'tab1': {
        templateUrl: 'templates/pacienteNoEmbarazada.html',
        controller: 'pacienteNoEmbarazadaCtrl'
      }
    }
  })

  .state('olvidasteTuContraseA', {
    url: '/page13',
    templateUrl: 'templates/olvidasteTuContraseA.html',
    controller: 'olvidasteTuContraseACtrl'
  })

  .state('tabsController.genitalYSistemaReproductor', {
    url: '/page19',
    views: {
      'tab1': {
        templateUrl: 'templates/genitalYSistemaReproductor.html',
        controller: 'genitalYSistemaReproductorCtrl'
      }
    }
  })

  .state('tabsController.embarazo', {
    url: '/page23',
    views: {
      'tab1': {
        templateUrl: 'templates/embarazo.html',
        controller: 'embarazoCtrl'
      }
    }
  })

  .state('legal', {
    url: '/page25',
    templateUrl: 'templates/legal.html',
    controller: 'legalCtrl'
  })

  .state('ayuda', {
    url: '/page26',
    templateUrl: 'templates/ayuda.html',
    controller: 'ayudaCtrl'
  })

  .state('urgenciasPasadas', {
    url: '/page27',
    templateUrl: 'templates/urgenciasPasadas.html',
    controller: 'urgenciasPasadasCtrl'
  })

  .state('10119', {
    url: '/page28',
    templateUrl: 'templates/10119.html',
    controller: '10119Ctrl'
  })

  .state('19118', {
    url: '/page31',
    templateUrl: 'templates/19118.html',
    controller: '19118Ctrl'
  })

  .state('21117', {
    url: '/page32',
    templateUrl: 'templates/21117.html',
    controller: '21117Ctrl'
  })

$urlRouterProvider.otherwise('/page6')


});